package com.my;

import com.my.DAO.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Connection;

@WebListener
public class ContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext ctx = sce.getServletContext();
		String path = ctx.getRealPath("/WEB-INF/log4j2.log");
		System.setProperty("logFile", path);
		
		final Logger log = LogManager.getLogger(ContextListener.class);
		log.debug("path = " + path);
		
		// check if a connection present!
		Connection con = ConnectionPool.getInstance().getConnection();
		System.out.println("con ==> " + con);
		
		ctx.setAttribute("app", ctx.getContextPath());
	}

}
