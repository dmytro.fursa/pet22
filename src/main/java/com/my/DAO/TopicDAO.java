package com.my.DAO;

import java.sql.Connection;

public class TopicDAO {
    private static TopicDAO instance;
    private Connection connection;

    public static TopicDAO getInstance() throws Exception {
        if (instance == null) {
            instance = new TopicDAO();
            instance.init();
        }
        return instance;
    }

    private void init() throws Exception {
        connection = ConnectionPool.getInstance().getConnection();
    }


}
