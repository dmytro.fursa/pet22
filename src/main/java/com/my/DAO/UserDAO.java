package com.my.DAO;

import com.my.Entity.User;

import java.sql.*;

import static com.my.DAO.SQLQuery.CREATE_USER;
import static com.my.DAO.SQLQuery.GET_USER_BY_EMAIL;

public class UserDAO {

    private static UserDAO instance;
    private Connection connection;

    public static UserDAO getInstance() throws Exception {
        if (instance == null) {
            instance = new UserDAO();
            instance.init();
        }
        return instance;
    }

    private void init() throws Exception {
        connection = ConnectionPool.getInstance().getConnection();
    }

    public User getUserByEmail(String eMail) throws SQLException {
        User user = new User();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_BY_EMAIL);
            preparedStatement.setString(1, eMail);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user.setLogin(resultSet.getString("login"));
                user.setId(resultSet.getInt("id"));
                user.setPassword(resultSet.getString("password"));
                user.setUserName(resultSet.getString("user_name"));
                user.setBlocked(resultSet.getBoolean("is_blocked"));
            }

        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return user;
    }

    public boolean createUser(User user) throws SQLException {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_USER);
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setInt(3, 0);
            preparedStatement.setString(4, user.getUserName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }
}
