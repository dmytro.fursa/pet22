package com.my.DAO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatorOfFields {

    private static int passwordLength = 3;

    public void setPasswordLength(int recommendPassLength) {
        ValidatorOfFields.passwordLength = recommendPassLength;
    }

    public int getPasswordLengthLength() {
        return passwordLength;
    }

    public static boolean validate(String eMail, String pass) {
        return sheckingEmail(eMail) && checkPassLength(pass);
    }

    public static boolean validate(String eMail, String pass, String user) {
        if (sheckingEmail(eMail)){
            if (checkPassLength(pass)){
                return checkingNameOfUser(user);
            }
        }
        return false;
    }

    private static boolean checkingNameOfUser(String user) {
        return !(user.length() == 0);
    }

    private static boolean sheckingEmail(String eMail) {
        Pattern pat = Pattern.compile("^.+@[a-z]+\\.[a-z]{2,5}$");
        Matcher matcher = pat.matcher(eMail);
        return matcher.find();
    }

    private static boolean checkPassLength(String pass) {
        return pass.length() >= passwordLength;
    }

}
