package com.my.Entity;

public class Question {
    private int id;
    private String question;
    private CorrectAnswer correctAnswer;
    private WrongAnswer wrongAnswer;
    private Topic topic;

    public Question(String question, CorrectAnswer correctAnswer, WrongAnswer wrongAnswer, Topic topic) {
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.wrongAnswer = wrongAnswer;
        this.topic = topic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public CorrectAnswer getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(CorrectAnswer correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public WrongAnswer getWrongAnswer() {
        return wrongAnswer;
    }

    public void setWrongAnswer(WrongAnswer wrongAnswer) {
        this.wrongAnswer = wrongAnswer;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }
}
