package com.my.Entity;

public class User {
    private int id;
    private String login;
    private String password;
    private boolean isBlocked;
    private String userName;

    public User() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public User(String login, String password, boolean isBlocked) {
        this.login = login;
        this.password = password;
        this.isBlocked = isBlocked;
    }



    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public void setId(int id) {
        this.id = id;
    }
}
