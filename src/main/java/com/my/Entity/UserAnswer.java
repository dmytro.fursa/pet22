package com.my.Entity;

public class UserAnswer {
    private int userId;
    private int questionId;
    private boolean isRight;

    public UserAnswer(int userId, int questionId, boolean isRight) {
        this.userId = userId;
        this.questionId = questionId;
        this.isRight = isRight;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public boolean isRight() {
        return isRight;
    }

    public void setRight(boolean right) {
        isRight = right;
    }
}
