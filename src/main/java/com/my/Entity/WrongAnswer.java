package com.my.Entity;

public class WrongAnswer {
    private int id;
    private String answer;
    private String anotherAnswer;

    public WrongAnswer(String answer, String anotherAnswer) {
        this.answer = answer;
        this.anotherAnswer = anotherAnswer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnotherAnswer() {
        return anotherAnswer;
    }

    public void setAnotherAnswer(String anotherAnswer) {
        this.anotherAnswer = anotherAnswer;
    }
}
