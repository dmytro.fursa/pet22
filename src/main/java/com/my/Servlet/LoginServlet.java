package com.my.Servlet;

import com.my.DAO.CheckUserInBase;
import com.my.DAO.UserDAO;
import com.my.DAO.ValidatorOfFields;
import com.my.Entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String email = req.getParameter("email");
        String pass = req.getParameter("password");
        boolean validated = ValidatorOfFields.validate(email, pass);
        boolean isExist = CheckUserInBase.check(email);
        RequestDispatcher dispatcher = null;
        if (!validated) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            dispatcher = this.getServletContext().getRequestDispatcher("/validation_error.jsp");
            dispatcher.forward(req, resp);
        } else if (isExist) {
            User user = null;
            try {
                user = UserDAO.getInstance().getUserByEmail(email);
                session.setAttribute("user", user);
            } catch (Exception e) {
                e.printStackTrace();
            }
            dispatcher = this.getServletContext().getRequestDispatcher("/home.jsp");
            dispatcher.forward(req, resp);
        } else {
            dispatcher = this.getServletContext().getRequestDispatcher("/loginError.jsp");
            dispatcher.forward(req, resp);
        }
    }
}
