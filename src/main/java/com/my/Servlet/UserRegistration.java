package com.my.Servlet;


import com.my.DAO.CheckUserInBase;
import com.my.DAO.UserDAO;
import com.my.DAO.ValidatorOfFields;
import com.my.Entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/registration")
public class UserRegistration extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletContext ctx = getServletContext();
        RequestDispatcher dispatcher = null;
        HttpSession session = request.getSession();
        String user = request.getParameter("user");
        String email = request.getParameter("email");
        String pass = request.getParameter("password");
        boolean validated = ValidatorOfFields.validate(email, pass, user);
        boolean isExist = CheckUserInBase.check(email);

        if (!validated) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            dispatcher = this.getServletContext().getRequestDispatcher("/validation_error.jsp");
            dispatcher.forward(request, response);
        } else if (isExist) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            dispatcher = this.getServletContext().getRequestDispatcher("/errorRegistration.jsp");
            dispatcher.forward(request, response);
        } else if (request.getParameter("save") != null) {
            User newUser = new User();
            newUser.setUserName(user);
            newUser.setPassword(pass);
            newUser.setLogin(email);
            ctx.setAttribute("user", newUser);
            session.setAttribute("email", email);
            session.setAttribute("user", user);
            boolean res = false;
            try {
                res = UserDAO.getInstance().createUser(newUser);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (res) {
                dispatcher = this.getServletContext().getRequestDispatcher("/successRegistration.jsp");
                dispatcher.forward(request, response);
            } else {
                dispatcher = this.getServletContext().getRequestDispatcher("/errorRegistration.jsp");
                dispatcher.forward(request, response);
            }

        } else if (request.getParameter("cancel") != null) {
            dispatcher = this.getServletContext().getRequestDispatcher("/index.jsp");
            dispatcher.forward(request, response);
        }
    }
}
