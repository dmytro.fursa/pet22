<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<head>

</head>
<body style="margin:0; padding:0">
<div style="text-align: center;">
    <img src="images/bg.jpg" />
    <div class="container">
        <div class="headbanner">
            <h1 style="display:inline">
                Welcome ${sessionScope.user.userName}
            </h1>
        </div>
        <h1></h1>
        <a href="account.jsp"><H2>Go to my account</H2></a>
        <a href="courses.jsp"><H2>Chose course</H2></a>
        <div class="myfooter">
            <h3 style="font-weight:normal; padding-left:10px"></h3>
        </div>
    </div>
</div>

</body>
</html>

