<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/res/css/bootstrap.css">
<script src="${pageContext.request.contextPath}/res/js/bootstrap.bundle.js"></script>

<head>
    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>
<h2>Login Page</h2>

<form method="POST" action="login">
    <table>
        e-mail: <input type="name" name="email"><br>
        Password: <input type="password" name="password" size="10"><br>
        <input type="submit" value="Enter"/>
        <a href="index.jsp">Back</a>
        <br>
        <a href="registration.jsp">Registration</a>
    </table>
</form>

<%--<form method="POST" action="login">--%>
<%--    <div class="row g-3 align-items-center">--%>
<%--            <label for="exampleInputEmail1" class="col-form-label">Email address</label>--%>
<%--        <div class="col-auto">--%>
<%--            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--%>
<%--        </div>--%>
<%--        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>--%>
<%--    </div>--%>
<%--    </div>--%>
<%--    <div class="row g-3 align-items-center">--%>
<%--            <label for="inputPassword6" class="col-form-label">Password</label>--%>
<%--        <div class="col-auto">--%>
<%--            <input type="password" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline">--%>
<%--        </div>--%>
<%--        <div class="col-auto">--%>
<%--    <span id="passwordHelpInline" class="form-text">--%>
<%--      Must be 8-20 characters long.--%>
<%--    </span>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--    <div class="mb-3 form-check">--%>
<%--        <input type="checkbox" class="form-check-input" id="exampleCheck1">--%>
<%--        <label class="form-check-label" for="exampleCheck1">Check me out</label>--%>
<%--    </div>--%>
<%--    <button type="submit" class="btn btn-primary">Submit</button>--%>
<%--</form>--%>


</body>