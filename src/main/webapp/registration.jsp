<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<head>
    <title>Title</title>
</head>
<body>
<div class="mycontent">
    <div class="space"><span><a class="formtext"><H2>Registration</H2></a></span></div>
    <div class="formcontent">
        <form action="registration" method="post">
            <table>
                <tr>
                    <td class="formtext">Username :</td>
                    <td><input name="user" type="text" size="30" /></td><td><a style="font-size: 10px; font-family:lucida sans; color: darkgray"></a></td>
                </tr>
                <tr>
                    <td class="formtext">E-mail :</td>
                    <td><input name="email" type="name" size="30" /></td><td><a style="font-size: 10px; font-family:lucida sans; color: darkgray"></a></td>
                </tr>
                <tr>
                    <td class="formtext">Password :</td>
                    <td><input name="password" type="password" size="30" /></td><td><a style="font-size: 10px; font-family:lucida sans; color: darkgray"></a></td>
                </tr>
                <table>
                    <tr>
                        <th>
                            <small>
                                <input type="submit" name="save" value="Save">
                            </small>
                        <th>
                            <small>
                                <input type="submit" name="cancel" value="Cancel">
                            </small>
                </table>
            </table>
        </form>
    </div>
</div>
</body>
</html>

